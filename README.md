##Sony Translation API

Runnning sample can be viewed on [167.114.249.23](http://167.114.249.23)

###Installation
The service is written in PHP 7.0.17 using Phalcon Framework 3.0.

###Prerequisites

- [Composer](https://getcomposer.org/)
- [Phalcon 3.0 +](https://phalconphp.com/en/download/linux)
- [Phalcon Devtools](https://github.com/phalcon/phalcon-devtools)

###Database Configuration

You can edit app/config/config.php for your configuration. You can also use project's cli tool to create new config file. The cli tool is named cli. You can run ./cli install command in project folder and enter your database connection information. A config.php file will be generated.

###Database Migration

You'll need to use [phalcon migration](https://docs.phalconphp.com/en/3.0.1/reference/migrations.html) tool with in order to migrate database. Simply run phalcon migration run command to migrate.

###User Creation

You can use project's cli tool to create user. The cli tool is named cli. You can run ./cli user command in project folder to create new user.

###Default Users

| Role | Email | Password |
| --- | --- | --- |
| Admin | admin@sony.com | asd123 |
| Agency | agency@sony.com | asd123 |
| Consumer | user@sony.com | asd123 |


### Step by step installation (for Debian 8)

``` 

sudo apt-get update
sudo apt-get install nginx wget git curl
 
echo 'deb http://packages.dotdeb.org jessie all' | sudo tee --append /etc/apt/sources.list
echo 'deb-src http://packages.dotdeb.org jessie all' | sudo tee --append /etc/apt/sources.list
 
curl -L "https://www.dotdeb.org/dotdeb.gpg" 2> /dev/null | sudo apt-key add -
sudo apt-get update
 
sudo apt-get install -y gcc make re2c libpcre3-dev php7.0-dev build-essential php7.0-zip php7.0-cli php7.0-common php7.0-fpm php7.0-dev php7.0-mcrypt php7.0-curl php7.0-xdebug php7.0-mysql
  
wget http://dev.mysql.com/get/mysql-apt-config_0.7.3-1_all.deb
sudo dpkg -i mysql-apt-config_0.7.3-1_all.deb
sudo apt-get install mysql-server
 
curl -s "https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh" | sudo bash
sudo apt-get install php7.0-phalcon
sudo service php7.0-fpm restart
 
wget https://getcomposer.org/download/1.4.1/composer.phar
chmod +x composer.phar
sudo mv composer.phar /usr/local/bin/composer
 
wget https://phar.phpunit.de/phpunit-5.7.phar
chmod +x phpunit-5.7.phar
sudo mv phpunit-5.7.phar /usr/local/bin/phpunit
 
cd /var/www/
git clone https://bahadirbirsoz@bitbucket.org/bahadirbirsoz/restapi.git
 
cd /var/www/restapi
composer install
 
```


### Running unit tests : 
```
cd /var/www/restapi/tests
phpunit . --bootstrap bootstrap.php --whitelist ../app/controllers/
```

###Nginx Configuration :
Basic nginx configuration on [Phalcon Documentation](https://docs.phalconphp.com/en/3.0.0/reference/nginx.html) is applicable.

Working example : 
```
server {
	listen 80 default_server;
	listen [::]:80 default_server;

	root /var/www/restapi/public;
	index index.php index.html;

	server_name _;

	location / {
	    try_files $uri $uri/ /index.php?_url=$uri&$args;
	}

	location ~ /\.git{
		deny all;
	}

	location ~ /\_cgi-bin{
		deny all;
	}

	location ~ \.php$ {
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		try_files $fastcgi_script_name =404;
		set $path_info $fastcgi_path_info;
		fastcgi_param PATH_INFO $path_info;
		fastcgi_index /index.php;
		include fastcgi.conf;
	}
}
```
