<?php

ini_set("display_errors", 1);

error_reporting(E_ALL);

define("ROOT_PATH", __DIR__);

define('APP_PATH', realpath('..'));

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// Required for phalcon/incubator
include __DIR__ . "/../vendor/autoload.php";


/**
 * Read the configuration
 */
$config = include APP_PATH . "/app/config/config.php";

/**
 * Read auto-loader
 */
include APP_PATH . "/app/config/loader.php";

