<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 04:34
 */

namespace Sony\Translate\Test\Controllers;

use Phalcon\Text;
use Sony\Translate\Controllers\Api\TranslateController;
use Sony\Translate\Models\Language;
use Sony\Translate\Models\Translation;
use Sony\Translate\Models\User;
use Sony\Translate\Test\UnitTestCase;

class TranslateTest extends UnitTestCase
{

    /**
     * @covers \Sony\Translate\Controllers\Api\TranslateController::post()
     */
    public function testPostActionSuccess()
    {
        $languages = Language::find();

        $user = User::findFirst();
        // Creating data to be tested
        $langSource = $languages[0];
        $langTarget = $languages[1];
        $source = Text::random(Text::RANDOM_ALNUM, 128);
        $target = Text::random(Text::RANDOM_ALNUM, 128);
        $controller = new TranslateController();

        $insert = new Translation();
        $insert->source = $source;
        $insert->source_language_id = $langSource->language_id;
        $insert->target_language_id = $langTarget->language_id;
        $insert->status = Translation::STATUS_PENDING;

        //Start the transaction
        $controller->db->begin();

        $insert->save();
        $data = (object)[
            'sourceLanguageCode' => $langSource->code,
            'targetLanguageCode' => $langTarget->code,
            'sourceText' => $source,
            'targetText' => $target
        ];

        $controller->auth->setUser($user);

        $controller->post($data);

        $translated = Translation::findFirst($insert->translation_id);

        $this->assertEquals($target, $translated->target, 'Translated Value');

        //Rollback the transaction
        $controller->db->rollback();

    }


    /**
     * @covers \Sony\Translate\Controllers\Api\TranslateController::post()
     */
    public function testPostNotFound()
    {
        $languages = Language::find();

        $user = User::findFirst();
        // Creating data to be tested
        $langSource = $languages[0];
        $langTarget = $languages[1];
        $source = Text::random(Text::RANDOM_ALNUM, 128);
        $target = Text::random(Text::RANDOM_ALNUM, 128);
        $controller = new TranslateController();


        //Start the transaction
        $controller->db->begin();

        $data = (object)[
            'sourceLanguageCode' => $langSource->code,
            'targetLanguageCode' => $langTarget->code,
            'sourceText' => $source,
            'targetText' => $target
        ];

        $controller->auth->setUser($user);

        $controller->post($data);

        $statusCode = (int)$controller->response->getStatusCode();
        $this->assertEquals(
            $statusCode,
            404,
            'Translation cannot be found'
        );

        //Rollback the transaction
        $controller->db->rollback();

    }

    /**
     * @covers \Sony\Translate\Controllers\Api\TranslateController::postAuth()
     */
    public function testCanAdminPost()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new TranslateController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Admin has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\TranslateController::postAuth()
     */
    public function testCanConsumerPost()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new TranslateController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Consumer has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\TranslateController::postAuth()
     */
    public function testCanAgencyPost()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new TranslateController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Agency has the auth to post");
    }


}