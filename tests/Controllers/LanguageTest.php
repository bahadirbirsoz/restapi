<?php

namespace Sony\Translate\Test\Controllers;


use Sony\Translate\Controllers\Api\LanguageController;
use Sony\Translate\Models\Language;
use Sony\Translate\Models\User;
use Sony\Translate\Test\UnitTestCase;

class LanguageTest extends UnitTestCase
{

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::get()
     */
    public function testGetAction()
    {
        $language = Language::findFirst();

        $controller = new LanguageController();
        $controller->get($language->code);

        $content = json_decode($controller->response->getContent());


        $this->assertEquals(
            '200 OK',
            $controller->response->getStatusCode(),
            "HTTP Status code is OK"
        );

        $this->assertEquals(
            200,
            $content->status,
            "Response Status code is OK"
        );

        $this->assertEquals(
            $language->language,
            $content->response->language,
            "Language name is OK"
        );

        $this->assertEquals(
            $language->code,
            $content->response->code,
            "Language code is OK"
        );
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::delete()
     */
    public function testDeleteAction()
    {
        $language = Language::findFirst();

        $controller = new LanguageController();
        $controller->db->begin();

        $controller->delete($language->code);
        $deleted = Language::countByCode($language->code);
        $controller->db->rollback();

        $this->assertNotInstanceOf(Language::class, $deleted);
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::postAuth()
     */
    public function testCanAdminPost()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Admin has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::postAuth()
     */
    public function testCanConsumerPost()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Consumer has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::postAuth()
     */
    public function testCanAgencyPost()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Agency has the auth to post");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::getAuth()
     */
    public function testCanAdminGet()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Admin has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::getAuth()
     */
    public function testCanConsumerGet()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Consumer has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::getAuth()
     */
    public function testCanAgencyGet()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Agency has the auth to get");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::deleteAuth()
     */
    public function testCanAdminDelete()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->deleteAuth(), "Admin has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::deleteAuth()
     */
    public function testCanConsumerDelete()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->deleteAuth(), "Consumer has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\LanguageController::deleteAuth()
     */
    public function testCanAgencyDelete()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new LanguageController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->deleteAuth(), "Agency has the auth to delete");
    }


}