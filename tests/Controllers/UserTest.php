<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 04:33
 */

namespace Sony\Translate\Test\Controllers;

use Phalcon\Text;
use Sony\Translate\Controllers\Api\UserController;
use Sony\Translate\Models\User;
use Sony\Translate\Test\UnitTestCase;

class UserTest extends UnitTestCase
{

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::get()
     */
    public function testGetAction()
    {
        $user = User::findFirst();

        $controller = new UserController();
        $controller->get($user->user_id);
        $content = json_decode($controller->response->getContent());
        $this->assertEquals($user->email, $content->response->email);

    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::delete()
     */
    public function testDeleteAction()
    {
        $user = User::findFirst();

        $controller = new UserController();
        $controller->db->begin();


        $controller->delete($user->user_id);

        $deleted = User::findFirst($user->user_id);
        $this->assertFalse($deleted);

        $controller->db->rollback();
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::put()
     */
    public function testPutAction()
    {
        $user = User::findFirst();

        $data = (object)[
            'email' => $user->email,
            'password' => Text::random(),
            'role' => User::ROLE_ADMIN
        ];

        $controller = new UserController();
        $controller->db->begin();

        $controller->put($data);
        $updated = $user = User::findFirst($user->user_id);

        $this->assertTrue($controller->security->checkHash($data->password, $updated->password));

        $controller->db->rollback();
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::post()
     */
    public function testPostAction()
    {

        $data = (object)[
            'email' => Text::random() . 'testcaseuser@sony.com',
            'password' => Text::random(),
            'role' => User::ROLE_ADMIN
        ];

        $controller = new UserController();
        $controller->db->begin();
        $controller->post($data);

        $recorded = User::findFirstByEmail($data->email);
        $this->assertEquals($recorded->role, $data->role);
        $this->assertTrue($controller->security->checkHash($data->password, $recorded->password));

        $controller->db->rollback();

    }


    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::postAuth()
     */
    public function testCanAdminPost()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Admin has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::postAuth()
     */
    public function testCanConsumerPost()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Consumer has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::postAuth()
     */
    public function testCanAgencyPost()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Agency has the auth to post");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::getAuth()
     */
    public function testCanAdminGet()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Admin has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::getAuth()
     */
    public function testCanConsumerGet()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->getAuth(), "Consumer has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::getAuth()
     */
    public function testCanAgencyGet()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->getAuth(), "Agency has the auth to get");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::deleteAuth()
     */
    public function testCanAdminDelete()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->deleteAuth(), "Admin has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::deleteAuth()
     */
    public function testCanConsumerDelete()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->deleteAuth(), "Consumer has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::deleteAuth()
     */
    public function testCanAgencyDelete()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->deleteAuth(), "Agency has the auth to delete");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::putAuth()
     */
    public function testCanAdminPut()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->putAuth(), "Admin has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::putAuth()
     */
    public function testCanConsumerPut()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->putAuth(), "Consumer has the auth to delete");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\UserController::putAuth()
     */
    public function testCanAgencyPut()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new UserController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->putAuth(), "Agency has the auth to delete");
    }


}