<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 04:33
 */

namespace Sony\Translate\Test\Controllers;


use Phalcon\Text;
use Sony\Translate\Controllers\Api\ValueController;
use Sony\Translate\Models\Language;
use Sony\Translate\Models\Translation;
use Sony\Translate\Test\UnitTestCase;
use Sony\Translate\Models\User;

class ValueTest extends UnitTestCase
{

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::getAction()
     * @covers \Sony\Translate\Controllers\Api\ValueController::get()
     */
    public function testGetAction()
    {
        $controller = new ValueController();
        $count = Translation::count([
            'conditions' => 'status = ?0',
            'bind' => [Translation::STATUS_READY]
        ]);

        $controller->get();

        $content = json_decode($controller->response->getContent());
        if ($count == 0) {
            $this->assertEquals(
                '404 Not Found',
                $controller->response->getStatusCode(),
                "HTTP Status code is OK"
            );
            $this->assertEquals(
                404,
                $content->status,
                "Response Status code is OK"
            );
        } else {
            $this->assertEquals(
                '200 OK',
                $controller->response->getStatusCode(),
                "HTTP Status code is 404"
            );
            $this->assertEquals(
                200,
                $content->status,
                "Response Status code is OK"
            );
        }

        $this->assertEquals(
            $count,
            count($content->response),
            "Response count checks out"
        );
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::postAction()
     * @covers \Sony\Translate\Controllers\Api\ValueController::post()
     */
    public function testPostAction()
    {
        $controller = new ValueController();

        $controller->db->begin();

        $languages = Language::find();
        $targetLang = $languages->getFirst();
        $languages->next();
        $sourceLang = $languages->current();
        $value = Text::random(Text::RANDOM_ALNUM, 128);

        $data = (object)[
            'source' => $sourceLang->code,
            'target' => $targetLang->code,
            'value' => $value
        ];

        $controller->post($data);

        $addedRow = Translation::findFirst([
            'conditions' => 'source = ?0 AND source_language_id = ?1 AND target_language_id = ?2',
            'bind' => [$value, $sourceLang->language_id, $targetLang->language_id]
        ]);

        $controller->db->rollback();

        $controller->get();

        $content = json_decode($controller->response->getContent());

        $this->assertEquals(
            '200 OK',
            $controller->response->getStatusCode(),
            "HTTP Status code is OK"
        );

        $this->assertEquals(
            200,
            $content->status,
            "Response Status code is OK"
        );

        $this->assertEquals(
            Translation::STATUS_PENDING,
            $addedRow->status,
            "Status for posted value"
        );

    }


    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::postAuth()
     */
    public function testCanAdminPost()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Admin has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::postAuth()
     */
    public function testCanConsumerPost()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->postAuth(), "Consumer has the auth to post");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::postAuth()
     */
    public function testCanAgencyPost()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->postAuth(), "Agency has the auth to post");
    }


    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::getAuth()
     */
    public function testCanAdminGet()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Admin has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::getAuth()
     */
    public function testCanConsumerGet()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Consumer has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\ValueController::getAuth()
     */
    public function testCanAgencyGet()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new ValueController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->getAuth(), "Agency has the auth to get");
    }


}