<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 04:33
 */

namespace Sony\Translate\Test\Controllers;

use Sony\Translate\Controllers\Api\WaitingController;
use Sony\Translate\Models\Translation;
use Sony\Translate\Test\UnitTestCase;
use Sony\Translate\Models\User;

class WaitingTest extends UnitTestCase
{

    /**
     * @covers \Sony\Translate\Controllers\Api\WaitingController::get()
     */
    public function testGetAction()
    {
        $count = Translation::count([
            'conditions' => 'status = ?0',
            'bind' => [Translation::STATUS_PENDING]
        ]);

        $controller = new WaitingController();
        $controller->get();

        $content = json_decode($controller->response->getContent());

        $this->assertEquals(
            '200 OK',
            $controller->response->getStatusCode(),
            "HTTP Status code is OK"
        );

        $this->assertEquals(
            200,
            $content->status,
            "Response Status code is OK"
        );

        $this->assertEquals(
            $count,
            count($content->response),
            "Response count checks out"
        );

    }


    /**
     * @covers \Sony\Translate\Controllers\Api\WaitingController::getAuth()
     */
    public function testCanAdminGet()
    {
        $user = new User();
        $user->role = User::ROLE_ADMIN;
        $contoller = new WaitingController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Admin has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\WaitingController::getAuth()
     */
    public function testCanConsumerGet()
    {
        $user = new User();
        $user->role = User::ROLE_CONSUMER;
        $contoller = new WaitingController();
        $contoller->auth->setUser($user);
        $this->assertFalse($contoller->getAuth(), "Consumer has the auth to get");
    }

    /**
     * @covers \Sony\Translate\Controllers\Api\WaitingController::getAuth()
     */
    public function testCanAgencyGet()
    {
        $user = new User();
        $user->role = User::ROLE_AGENCY;
        $contoller = new WaitingController();
        $contoller->auth->setUser($user);
        $this->assertTrue($contoller->getAuth(), "Agency has the auth to get");
    }


}