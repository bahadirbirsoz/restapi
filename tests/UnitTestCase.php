<?php

namespace Sony\Translate\Test;

use Phalcon\Di;
use Phalcon\Test\UnitTestCase as PhalconTestCase;
use Sony\Translate\Library\Response;

abstract class UnitTestCase extends PhalconTestCase
{
    /**
     * @var bool
     */
    private $_loaded = false;

    protected $response;

    public function setUp()
    {
        parent::setUp();

        /**
         * Read the configuration
         */
        $config = include APP_PATH . "/app/config/config.php";

        /**
         * Read auto-loader
         */
        include APP_PATH . "/app/config/loader.php";

        /**
         * Read services
         */
        $di = include APP_PATH . "/app/config/services.php";

        $this->setDI($di);

        $this->response = new Response();

        $this->_loaded = true;
    }

    /**
     * Check if the test case is setup properly
     *
     * @throws \PHPUnit_Framework_IncompleteTestError;
     */
    public function __destruct()
    {
        if (!$this->_loaded) {
            throw new \PHPUnit_Framework_IncompleteTestError(
                "Please run parent::setUp()."
            );
        }
    }
}

