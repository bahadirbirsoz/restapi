<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class TranslationMigration_101
 */
class TranslationMigration_101 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('translation', array(
                'columns' => array(
                    new Column(
                        'translation_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 10,
                            'first' => true
                        )
                    ),
                    new Column(
                        'source_user_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'after' => 'translation_id'
                        )
                    ),
                    new Column(
                        'translate_user_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'after' => 'source_user_id'
                        )
                    ),
                    new Column(
                        'source_language_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'after' => 'translate_user_id'
                        )
                    ),
                    new Column(
                        'target_language_id',
                        array(
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'size' => 10,
                            'after' => 'source_language_id'
                        )
                    ),
                    new Column(
                        'source',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'target_language_id'
                        )
                    ),
                    new Column(
                        'target',
                        array(
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'source'
                        )
                    ),
                    new Column(
                        'status',
                        array(
                            'type' => Column::TYPE_CHAR,
                            'default' => "pending",
                            'size' => 1,
                            'after' => 'target'
                        )
                    )
                ),
                'indexes' => array(
                    new Index('PRIMARY', array('translation_id'), 'PRIMARY'),
                    new Index('source_language_id', array('source_language_id'), null),
                    new Index('target_language_id', array('target_language_id'), null),
                    new Index('source_user_id', array('source_user_id'), null),
                    new Index('translate_user_id', array('translate_user_id'), null)
                ),
                'references' => array(
                    new Reference(
                        'translation_ibfk_1',
                        array(
                            'referencedSchema' => 'translation',
                            'referencedTable' => 'language',
                            'columns' => array('source_language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'translation_ibfk_2',
                        array(
                            'referencedSchema' => 'translation',
                            'referencedTable' => 'language',
                            'columns' => array('target_language_id'),
                            'referencedColumns' => array('language_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'translation_ibfk_3',
                        array(
                            'referencedSchema' => 'translation',
                            'referencedTable' => 'user',
                            'columns' => array('source_user_id'),
                            'referencedColumns' => array('user_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    ),
                    new Reference(
                        'translation_ibfk_4',
                        array(
                            'referencedSchema' => 'translation',
                            'referencedTable' => 'user',
                            'columns' => array('translate_user_id'),
                            'referencedColumns' => array('user_id'),
                            'onUpdate' => '',
                            'onDelete' => ''
                        )
                    )
                ),
                'options' => array(
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '155',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ),
            )
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
