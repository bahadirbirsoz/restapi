<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */

if (php_sapi_name() == "cli") {
    $di = new Phalcon\Di\FactoryDefault\Cli();

    $di->set('dispatcher', function () {
        $dispatcher = new Phalcon\CLI\Dispatcher();
        $dispatcher->setDefaultNamespace('Sony\\Translate\\Tasks');
        return $dispatcher;
    });

} else {
    $di = new Phalcon\Di\FactoryDefault();

    $di->set('router', function () use ($config) {
        return require(__DIR__ . '/router.php');
    });

}

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($config) {
    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    $view->registerEngines([
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;
    return new $class($dbConfig);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

$di->setShared('response', function () {
    $response = new \Sony\Translate\Library\Response();
    $response->setStatusCode(200);
    return $response;
});

$di->set('auth', function () {
    return new \Sony\Translate\Library\Auth();
});

Phalcon\Di::setDefault($di);

return $di;


