<?php

$router = new Phalcon\Mvc\Router();
$router->clear();
$router->removeExtraSlashes(true);
$router->setDefaultNamespace('Sony\Translate\Controllers');

$router->addGet(
    '/api/:controller', [
    'namespace' => 'Sony\\Translate\\Controllers\\Api',
    'controller' => 1,
    'action' => 'get',
]);

$router->addGet(
    '/api/:controller/:params', [
    'namespace' => 'Sony\\Translate\\Controllers\\Api',
    'controller' => 1,
    'action' => 'get',
    'params' => 2
]);

$router->addPost(
    '/api/:controller', [
    'namespace' => 'Sony\\Translate\\Controllers\\Api',
    'controller' => 1,
    'action' => 'post',
]);
$router->addPut(
    '/api/:controller', [
    'namespace' => 'Sony\\Translate\\Controllers\\Api',
    'controller' => 1,
    'action' => 'put',
]);
$router->addDelete(
    '/api/:controller/:params', [
    'namespace' => 'Sony\\Translate\\Controllers\\Api',
    'controller' => 1,
    'action' => 'delete',
    'params' => 2
]);

$router->addGet('/', [
    'namespace' => 'Sony\\Translate\\Controllers\\Doc',
    'controller' => 'service',
    'action' => 'index'
]);

$router->addGet('/service', [
    'namespace' => 'Sony\\Translate\\Controllers\\Doc',
    'controller' => 'service',
    'action' => 'index'
]);

$router->addGet('/service/:action', [
    'namespace' => 'Sony\\Translate\\Controllers\\Doc',
    'controller' => 'service',
    'action' => 1
]);

return $router;
