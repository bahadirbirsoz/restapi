<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
    array(
        'Sony\\Translate\\Controllers' => $config->application->controllersDir,
        'Sony\\Translate\\Models' => $config->application->modelsDir,
        'Sony\\Translate\\Library' => $config->application->libraryDir,
        'Sony\\Translate\\Test' => $config->application->testsDir,
        'Sony\\Translate\\Tasks' => $config->application->tasksDir,
    )
)->register();
