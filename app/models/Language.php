<?php

namespace Sony\Translate\Models;

use Phalcon\Mvc\Model;

class Language extends Model
{

    /**
     *
     * @var integer
     */
    public $language_id;

    /**
     *
     * @var string
     */
    public $language;

    /**
     *
     * @var string
     */
    public $code;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('language_id', 'Sony\Translate\Models\Translation', 'source_language_id', array('alias' => 'Translation'));
        $this->hasMany('language_id', 'Sony\Translate\Models\Translation', 'target_language_id', array('alias' => 'Translation'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Language[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Language
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'language';
    }

}
