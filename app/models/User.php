<?php

namespace Sony\Translate\Models;

use Phalcon\Mvc\Model;


/**
 * Class User
 * @package Sony\Translate\Models
 */
class User extends Model
{
    CONST ROLE_ADMIN = 'admin';
    CONST ROLE_AGENCY = 'agency';
    CONST ROLE_CONSUMER = 'consumer';

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var string
     */
    public $role;

    /**
     *
     * @var string
     */
    public $time_register;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('user_id', 'Sony\Translate\Models\Translation', 'source_user_id', array('alias' => 'Translation'));
        $this->hasMany('user_id', 'Sony\Translate\Models\Translation', 'translate_user_id', array('alias' => 'Translation'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return User[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return User
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'user';
    }

    /**
     * Hooks the create event before validation to set time_register
     */
    public function beforeValidationOnCreate()
    {
        $this->time_register = date("Y-m-d H:i:s");
    }

}
