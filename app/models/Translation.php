<?php

namespace Sony\Translate\Models;

use Phalcon\Mvc\Model;


class Translation extends Model
{

    CONST STATUS_PENDING = 'pending';
    CONST STATUS_READY = 'ready';
    /**
     *
     * @var integer
     */
    public $translation_id;

    /**
     *
     * @var integer
     */
    public $source_user_id;

    /**
     *
     * @var integer
     */
    public $translate_user_id;

    /**
     *
     * @var integer
     */
    public $source_language_id;

    /**
     *
     * @var integer
     */
    public $target_language_id;

    /**
     *
     * @var string
     */
    public $source;

    /**
     *
     * @var string
     */
    public $target;

    /**
     *
     * @var string
     */
    public $status;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('source_language_id', 'Sony\Translate\Models\Language', 'language_id', array('alias' => 'Language'));
        $this->belongsTo('target_language_id', 'Sony\Translate\Models\Language', 'language_id', array('alias' => 'Language'));
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translation[]
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Translation
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * @return Translation[]
     */
    public static function findPending()
    {
        return static::findByStatus(self::STATUS_PENDING);
    }

    /**
     * @return Translation[]
     */
    public static function findReady()
    {
        return static::findByStatus(self::STATUS_READY);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'translation';
    }

}
