<?php

namespace Sony\Translate\Library\Validator;

use Sony\Translate\Models\Language;

class LanguageCode extends \Phalcon\Validation\Validator\Callback
{
    public function __construct($fieldName)
    {
        $options = [
            'message' => 'Invalid language code name for :field',
            'callback' => function ($data) use ($fieldName) {
                if (!isset($data->$fieldName)) {
                    return false;
                }
                $code = $data->$fieldName;
                $language = Language::findFirstByCode($code);
                if ($language) {
                    return true;
                }
                return false;
            }
        ];
        parent::__construct($options);
    }
}