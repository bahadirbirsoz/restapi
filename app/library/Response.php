<?php

namespace Sony\Translate\Library;

use Phalcon\Validation\Message\Group;

/**
 * Class Response
 * @package Sony\Translate\Library
 *
 * Handles all HTTP Responses.
 */
class Response extends \Phalcon\Http\Response
{

    protected $isContentSet = false;

    protected $defaultContent;

    /**
     * Sets default content.
     * @param $content
     */
    public function setDefaultContent($content)
    {
        $this->defaultContent = $content;
    }

    /**
     * @param string $message
     * @return bool
     */
    public function errNotFound(string $message = "Not Found")
    {
        $this->setStatusCode(404);
        $this->setJsonContent([
            'message' => $message
        ]);
        return false;
    }

    /**
     * This method handles error messages after validation fails.
     *
     * @param Group $messages
     * @param int $statusCode
     */
    public function errMessages(Group $messages, int $statusCode = 400)
    {
        $this->setStatusCode($statusCode);
        $content = [];
        foreach ($messages as $message) {
            $content[] = [
                'field' => $message->getField(),
                'message' => $message->getMessage()
            ];
        }
        $this->setJsonContent($content);
    }

    /**
     * General purpose error response handler.
     * @param string $message
     * @param string $field
     * @param int $statusCode
     */
    public function error(string $message, string $field = "", int $statusCode = 400)
    {
        $this->setStatusCode($statusCode);
        $this->setJsonContent([
            'field' => $field,
            'message' => $message
        ]);
    }

    /**
     * Handles response when user is not authorized to perform requested method.
     */
    public function errAuthFailed()
    {
        $this->setStatusCode(401);
        $this->setJsonContent([
            'field' => 'Authorization',
            'message' => 'You do not have authorization to perform given action.'
        ]);
    }

    /**
     * Handles response when given authorization information is cannot be found in database or invalid.
     */
    public function errForbidden()
    {
        $this->setStatusCode(403);
        $this->setJsonContent([
            'field' => 'Authorization',
            'message' => 'Given authorization information is invalid.'
        ]);
    }

    /**
     * Handles response when an invalid action has been requested.
     * @param string $method
     */
    public function errInvalidAction($method = "method")
    {
        $this->setStatusCode(405);
        $this->setJsonContent([
            'field' => 'Method',
            'message' => 'Given method (' . $method . ') is not allowed for the requested resource'
        ]);
    }

    /**
     * Handles response when request body for given method cannot be handled (Invalid JSON or x-http form data)
     */
    public function errInvalidRequest()
    {
        $this->setStatusCode(400);
        $this->setJsonContent([
            'field' => 'Request',
            'message' => 'Server was not able process the request body'
        ]);
    }

    /**
     * Handles general purpose success messages
     * @param $message
     */
    public function success($message)
    {
        $this->setStatusCode(200);
        $this->setJsonContent([
            'message' => $message
        ]);
    }

    /**
     * @param mixed $content
     * @param int $jsonOptions
     * @param int $depth
     */
    public function setJsonContent($content, $jsonOptions = 0, $depth = 512)
    {
        $this->isContentSet = true;
        $content = [
            'status' => (int)$this->getStatusCode(),
            'response' => $content
        ];

        parent::setJsonContent($content, $jsonOptions, $depth);
    }

    /**
     * Sends the response to client.
     * If there is no content created for given action, default content will be set just before sending
     */
    public function send()
    {
        if (!$this->isContentSet) {
            $this->setJsonContent($this->defaultContent);
        }
        parent::send();
    }

}

