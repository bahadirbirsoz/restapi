<?php
/**
 * Created by PhpStorm.
 * User: Bahadır Birsöz<bahadirbirsoz@gmail.com>
 * Date: 25.03.2017
 * Time: 12:00
 */

namespace Sony\Translate\Library;

use Phalcon\Mvc\User\Component;
use Sony\Translate\Models\User;

/**
 * Class Auth
 * Handles HTTP Basic Authentication
 * @package Sony\Translate\Library
 */
class Auth extends Component
{

    /**
     * @var User
     */
    protected $user = false;

    /**
     * Auth constructor. Gets the auth header if exists.
     * If there is no auth header, client will be treated as guest.
     * If there is auth header, checks for authentication.
     * Sets the response as Forbidden (403)
     */
    public function __construct()
    {
        if (@$this->request) {
            $auth = $this->request->getBasicAuth();
            if (!is_array($auth)) {
                return;
            }
            /* @var $user User */
            $user = User::findFirstByEmail($auth['username']);
            if ($this->security->checkHash($auth['password'], $user->password)) {
                $this->user = $user;
                return;
            }
        }
        $this->response->errForbidden();
    }

    /**
     * Sets the User model for overriding. This method is to be used only in tests.
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Returns User Object for current user
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Returns true if user is valid and admin, false otherwise
     * @return bool
     */
    public function isAdmin()
    {
        return $this->check() && $this->user->role === User::ROLE_ADMIN;
    }

    /**
     * Returns true if user is valid and agency, false otherwise
     * @return bool
     */
    public function isAgency()
    {
        return $this->check() && $this->user->role === User::ROLE_AGENCY;
    }

    /**
     * Returns true if user is valid and consumer, false otherwise
     * @return bool
     */
    public function isConsumer()
    {
        return $this->check() && $this->user->role === User::ROLE_CONSUMER;
    }

    /**
     * Returns true if user is valid, false otherwise
     * @return bool
     */
    public function check()
    {
        return $this->user !== false;
    }


}