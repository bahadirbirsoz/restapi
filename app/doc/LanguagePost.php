<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 13:47
 */

namespace Sony\Translate\Doc;

/**
 * Class LanguagePost
 * @package Sony\Translate\Doc
 *
 * This class is for documentation purpose only. An instance will never be created.
 * POST /language endpoint's request body will be interpreted as such
 */
abstract class LanguagePost
{
    /**
     * @var string Corresponds to \Sony\Translate\Models\Language::$code
     */
    public $code;

    /**
     * @var string Corresponds to \Sony\Translate\Models\Translate::$language
     */
    public $language;

}