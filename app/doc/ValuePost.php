<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 00:29
 */

namespace Sony\Translate\Doc;

/**
 * Class ValuePost
 * @package Sony\Translate\Doc
 *
 * This class is for documentation purpose only. An instance will never be created.
 * POST /value endpoint's request body will be interpreted as such
 */
abstract class ValuePost
{

    /**
     * @var string Source Language Code, 2 chars string, eg('en', 'tr')
     * Corresponds to \Sony\Translate\Models\Language::$code
     */
    public $source;

    /**
     * @var string Target Language Code, 2 chars string, eg('en', 'tr')
     * Corresponds to \Sony\Translate\Models\Language::$code
     */
    public $target;

    /**
     * @var string Text to be translated
     * Corresponds to \Sony\Translate\Models\Translation::$source
     */
    public $value;

}