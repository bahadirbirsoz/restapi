<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 00:29
 */

namespace Sony\Translate\Doc;

/**
 * Class TranslatePost
 * @package Sony\Translate\Doc
 *
 * This class is for documentation purpose only. An instance will never be created.
 * POST /translation endpoint's request body will be interpreted as such
 */
abstract class TranslatePost
{
    /**
     * @var string Corresponds to \Sony\Translate\Models\Language::$code
     */
    public $sourceLanguageCode;

    /**
     * @var string Corresponds to \Sony\Translate\Models\Language::$code
     */
    public $targetLanguageCode;

    /**
     * @var string Corresponds to \Sony\Translate\Models\Translate::$source
     */
    public $sourceText;

    /**
     * @var string Corresponds to \Sony\Translate\Models\Translate::$target
     */
    public $targetText;
}