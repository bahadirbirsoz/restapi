<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 12:44
 */

namespace Sony\Translate\Doc;

/**
 * Class UserPost
 * @package Sony\Translate\Doc
 *
 * This class is for documentation purpose only. An instance will never be created.
 * POST /user endpoint's request body will be interpreted as such
 */
abstract class UserPost
{
    /**
     * Corresponds to \Sony\Translate\Models\User::$email
     * @var string Email of the user
     */
    public $email;

    /**
     * Corresponds to \Sony\Translate\Models\User::$password
     * @var string Password of the user
     */
    public $password;

    /**
     * Corresponds to \Sony\Translate\Models\User::$role
     * @var string Role of the user (admin|agency|consumer)
     */
    public $role;
}