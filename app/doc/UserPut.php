<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 26.03.2017
 * Time: 22:55
 */

namespace Sony\Translate\Doc;


/**
 * Class UserPut
 * @package Sony\Translate\Doc
 *
 * This class is for documentation purpose only. An instance will never be created.
 * PUT /user endpoint's request body will be interpreted as such
 */
abstract class UserPut
{
    /**
     * Corresponds to \Sony\Translate\Models\User::$email
     * @var string Email of the user
     */
    public $email;

    /**
     * Corresponds to \Sony\Translate\Models\User::$password
     * @var string Password of the user
     */
    public $password;

    /**
     * Corresponds to \Sony\Translate\Models\User::$role
     * @var string Role of the user (admin|agency|consumer)
     */
    public $role;
}