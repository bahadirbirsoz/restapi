<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 17:08
 */

namespace Sony\Translate\Tasks;

use Phalcon\Cli\Task;
use Phalcon\Db\Adapter\Pdo;

/**
 * Class InstallTask
 * @package Sony\Translate\Tasks
 *
 * This is a cli task for preparing config file
 */
class InstallTask extends Task
{

    protected $config = array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'dbname' => 'translation',
        'charset' => 'utf8',
    );

    static $adapters = [
        'Mysql', 'Postgresql', 'Sqlite', 'Oracle'
    ];

    public function mainAction()
    {
        $first = true;
        do {
            if (!$first) {
                echo "Sorry, a database connection could not be established with given arguments. Please check again.\n\n";
            }
            $first = false;

            $this->getConnectionParams();
        } while (!$this->canConnect());

        $this->save();
    }

    /**
     * Checks if current connection arguments are valid and working
     * @return bool
     */
    protected function canConnect()
    {
        if (!$this->config['username'] ||
            !$this->config['dbname'] ||
            !$this->config['host'] ||
            !$this->config['adapter']
        ) {
            return false;
        }

        try {
            $config = $this->config;
            $adapter = $this->config['adapter'];
            unset($config['adapter']);
            $class = '\Phalcon\Db\Adapter\Pdo\\' . $adapter;
            $conn = new $class($config);
            /* @var $conn Pdo */
            if ($conn) {
                echo "Database connection established successfully.\n";
                return true;
            }

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Reads connection arguments from user.
     */
    protected function getConnectionParams()
    {
        foreach ($this->config as $key => $val) {
            $newVal = readline("Please enter $key <" . $val . ">\n");
            if ($key === 'adapter' && $newVal) {
                $newVal = ucfirst(strtolower($newVal));
                while (!in_array($newVal, static::$adapters)) {
                    echo "Invalid adapter. Available engines are : \n" . implode(', ', static::$adapters) . "\n";
                    $newVal = readline("Please enter $key <" . $val . ">\n");
                    $newVal = ucfirst(strtolower($newVal));
                }
            }
            /**
             * Password can be empty
             */
            if ($newVal || $key == 'password') $this->config[$key] = $newVal;

        }
    }

    /**
     * Saves config file
     */
    protected function save()
    {
        $path = dirname(__DIR__) . '/config/config.php';
        $template = file_get_contents(__DIR__ . '/resources/config.php.template');
        foreach ($this->config as $key => $val) {
            $template = str_replace('{' . $key . '}', "'" . addslashes($val) . "'", $template);
        }
        if (!file_put_contents($path, $template)) {
            echo "Config file written to $path";
        } else {
            echo "Cannot write to $path.\n Please write following content to $path\n\n$template\n";

        }

    }

}