<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 28.03.2017
 * Time: 15:20
 */

namespace Sony\Translate\Tasks;


use Phalcon\Cli\Task;

class MainTask extends Task
{
    public function mainAction()
    {
        echo "Usage : \n
For creating user : 
 ./cli user
 
For creating config file :
 ./cli install \n\n";
    }
}