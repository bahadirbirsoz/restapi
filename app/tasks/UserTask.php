<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 27.03.2017
 * Time: 15:33
 */

namespace Sony\Translate\Tasks;

use Phalcon\Cli\Task;
use Phalcon\Validation;
use Sony\Translate\Controllers\Api\UserController;

/**
 * Class UserTask
 * @package Sony\Translate\Tasks
 *
 * This is a cli task for creating users
 */
class UserTask extends Task
{
    protected $data;

    protected $controller;

    protected $validation;

    protected $messages;

    public function mainAction()
    {
        $this->controller = new UserController();
        $this->validation = new Validation();
        $this->controller->postValidation($this->validation);

        $first = true;
        do {
            if (!$first) {
                echo "Sorry, given user information could not be validated. Please check again.\n\n";
                $group = $this->validation->getMessages();
                foreach ($group as $message) {
                    echo $message->getField() . " : " . $message->getMessage() . "\n";
                }
            }
            $first = false;

            $this->getArguments();
        } while (!$this->isValid());

        if ($this->controller->post($this->data)) {
            echo "\nUser created successfully\n";
        } else {
            echo "\nTask failed\n";
        }

        echo "\n";
    }

    /**
     * Checks if given user information validates
     * @return bool
     */
    protected function isValid()
    {
        $messages = $this->validation->validate($this->data);
        if ($messages->count() > 0) {
            return false;

        }
        return true;

    }

    /**
     * Gets user information to create from cli
     */
    protected function getArguments()
    {
        $email = readline("Enter user email address : \n");
        $password = readline("Enter user password : \n");
        $role = readline("Please enter user role (admin|agency|consumer) : \n");
        $this->data = (object)[
            'email' => $email,
            'password' => $password,
            'role' => $role
        ];
    }
}