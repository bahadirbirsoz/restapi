<?php

namespace Sony\Translate\Controllers\Api;

use Sony\Translate\Models\Language;
use Sony\Translate\Models\Translation;

/**
 * Class WaitingController
 * @package Sony\Translate\Controllers\Api
 */
class WaitingController extends Rest implements Rest\Get
{
    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function getAuth():bool
    {
        return $this->auth->isAdmin() || $this->auth->isAgency();
    }

    /**
     *
     * There is no need to use $id argument for this method, always returns a list of values
     *
     * @param null $id
     * @return bool
     */
    public function get($id = null):bool
    {
        $data = $this->modelsManager->createBuilder()
            ->addFrom(Translation::class, 'translation')
            ->join(Language::class, 'sourcelang.language_id = translation.source_language_id', 'sourcelang')
            ->join(Language::class, 'targetlang.language_id = translation.target_language_id', 'targetlang')
            ->columns([
                'translation.source as value',
                'sourcelang.code as source',
                'targetlang.code as target'
            ])
            ->where('translation.status = ?0', [Translation::STATUS_PENDING])
            ->getQuery()
            ->execute();

        if (!$data->count()) {
            $this->response->errNotFound();
            return false;
        }
        $this->response->setJsonContent($data->toArray());
        return true;
    }
}