<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 13:08
 */

namespace Sony\Translate\Controllers\Api;

use Phalcon\Validation;
use Sony\Translate\Doc\LanguagePost;
use Sony\Translate\Models\Language;

/**
 * Class LanguageController
 * @package Sony\Translate\Controllers\Api
 */
class LanguageController extends Rest implements Rest\Get, Rest\Post, Rest\Delete
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function deleteAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * @param $code
     * @return bool
     */
    public function delete($code):bool
    {
        Language::findFirstByCode($code)->delete();
        return true;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function getAuth():bool
    {
        return $this->auth->check();
    }

    /**
     * @param string|null $code
     * @return bool
     */
    public function get($code = null):bool
    {
        if (is_null($code)) {
            $data = Language::find();
        } else {
            $data = Language::findFirstByCode($code);
        }
        if (!$data || !$data->count()) {
            return $this->response->errNotFound();
        }
        $this->response->setJsonContent($data->toArray());
        return true;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function postAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * Handles language post operation.
     * @param $data \Sony\Translate\Doc\LanguagePost
     * @return boolean
     */
    public function post($data):bool
    {
        $language = new Language();
        $language->language = $data->language;
        $language->code = $data->code;
        if ($language->save()) {
            return true;
        }
        $this->response->errMessages($language->getMessages());
        return false;
    }

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function postValidation(Validation $validation):Validation
    {
        $validation->add('code', new Validation\Validator\PresenceOf());
        $validation->add('language', new Validation\Validator\PresenceOf());
        $validation->add('code', new Validation\Validator\Uniqueness([
            "model" => new Language(),
        ]));
        return $validation;
    }
}