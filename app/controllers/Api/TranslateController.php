<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 00:23
 */

namespace Sony\Translate\Controllers\Api;

use Phalcon\Validation;
use Sony\Translate\Doc\TranslatePost;
use Sony\Translate\Library\Validator\LanguageCode;
use Sony\Translate\Models\Language;
use Sony\Translate\Models\Translation;

/**
 * Class TranslateController
 * @package Sony\Translate\Controllers\Api
 */
class TranslateController extends Rest implements Rest\Post
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function postAuth():bool
    {
        return $this->auth->isAdmin() || $this->auth->isAgency();
    }

    /**
     * @param $data TranslatePost
     * @return bool
     */
    public function post($data):bool
    {
        /* @var $sourceLanguage Language */
        $sourceLanguage = Language::findFirstByCode($data->sourceLanguageCode);

        /* @var $targetLanguage Language */
        $targetLanguage = Language::findFirstByCode($data->targetLanguageCode);


        $translation = Translation::findFirst([
            'conditions' => 'source_language_id = ?0 AND target_language_id = ?1 AND source = ?2 AND status = ?3',
            'bind' => [
                $sourceLanguage->language_id,
                $targetLanguage->language_id,
                $data->sourceText,
                Translation::STATUS_PENDING
            ]
        ]);

        if (!$translation) {
            $this->response->errNotFound("Value to be translated cannot be found");
            return false;
        }

        $translation->target = $data->targetText;
        $translation->status = Translation::STATUS_READY;
        $translation->translate_user_id = $this->auth->getUser()->user_id;

        if ($translation->save()) {
            return true;
        }

        $this->response->errMessages($translation->getMessages());
        return false;
    }

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function postValidation(Validation $validation):Validation
    {
        $validation->add('sourceText', new Validation\Validator\PresenceOf());
        $validation->add('targetText', new Validation\Validator\PresenceOf());

        $validation->add('sourceLanguageCode', new Validation\Validator\PresenceOf());
        $validation->add('targetLanguageCode', new Validation\Validator\PresenceOf());

        $validation->add('sourceLanguageCode', new LanguageCode('sourceLanguageCode'));
        $validation->add('targetLanguageCode', new LanguageCode('sourceLanguageCode'));
        return $validation;
    }
}