<?php

namespace Sony\Translate\Controllers\Api;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Validation;

/**
 * Class Rest
 * @package Sony\Translate\Controllers
 * @property \Sony\Translate\Library\Response $response
 * @property \Sony\Translate\Library\Auth $auth
 */
abstract class Rest extends Controller
{

    /**
     * @var $validation Validation
     */
    protected $validation;

    protected $error;

    private $data;

    public function initialize()
    {
        $this->auth->check();
    }

    /**
     * Returns the request parameters as an object
     * @return object
     */
    protected function getData()
    {
        if (!isset($this->data)) {
            $this->setData();
        }
        return $this->data;
    }

    /**
     * Sets the request data. Parses json to object if request content type is application/json.
     * Default content type is assumed to be application/x-www-form-urlencoded
     */
    private function setData()
    {
        switch ($this->request->getHeader("Content-Type")) {
            case 'application/json':
                $this->data = json_decode($this->request->getRawBody());
                break;
            default:
                $this->data = json_decode(json_encode($this->request->getPost()));
                break;
            // workaroud for converting and array to object recursively
        }
    }

    /**
     * Handles get action : Checks for authentication, starts a transaction, calls controller::get method
     * commits the transaction if get method returns true, rollback the transaction otherwise.
     *
     * @return bool|void
     */
    public final function getAction($params = null)
    {
        if (!$this instanceof Rest\Get) {
            $this->response->errInvalidAction();
            return;
        }

        if (!$this->getAuth()) {
            return $this->response->errAuthFailed();
        }
        $this->db->begin();
        if ($this->get($params)) {
            return $this->db->commit();
        }
        return $this->db->rollback();
    }

    /**
     * Handles put action : Checks for authentication, starts a transaction, calls controller::put method
     * commits the transaction if put method returns true, rollback the transaction otherwise.
     *
     * @return bool|void
     */
    public final function putAction()
    {
        $this->response->setDefaultContent("Record updated successfully");
        if (!$this instanceof Rest\Put) {
            $this->response->errInvalidAction();
            return;
        }
        if (!$this->putAuth()) {
            return $this->response->errAuthFailed();
        }

        $data = $this->getData();

        if (!is_object($data)) {
            $this->response->errInvalidRequest();
            return;
        }

        $validation = new Validation();
        $this->putValidation($validation);
        $messages = $validation->validate($data);
        if (count($messages)) {
            $this->response->errMessages($messages);
            return;
        }
        $this->db->begin();

        if ($this->put($data)) {
            return $this->db->commit();
        }
        return $this->db->rollback();
    }

    /**
     * Handles delete action : Checks for authentication, starts a transaction, calls controller::delete method
     * commits the transaction if delete method returns true, rollback the transaction otherwise.
     *
     * @param $uniqeIdentifier
     * @return bool|void
     */
    public final function deleteAction($uniqeIdentifier)
    {
        $this->response->setDefaultContent("Record deleted successfully");
        if (!$this instanceof Rest\Delete) {
            $this->response->errInvalidAction();
            return;
        }

        if (!$this->deleteAuth()) {
            return $this->response->errAuthFailed();
        }
        $this->db->begin();
        if ($this->delete($uniqeIdentifier)) {
            return $this->db->commit();
        }
        return $this->db->rollback();
    }


    public final function postAction()
    {
        $this->response->setDefaultContent("Record inserted successfully");
        if (!$this instanceof Rest\Post) {
            $this->response->errInvalidAction(__METHOD__);
            return;
        }

        if (!$this->postAuth()) {
            return $this->response->errAuthFailed();
        }
        $data = $this->getData();

        if (!is_object($data)) {
            $this->response->errInvalidRequest();
            return;
        }

        $validation = new Validation();
        $this->postValidation($validation);

        $messages = $validation->validate($data);
        if (count($messages)) {
            $this->response->errMessages($messages);
            return;
        }

        $this->db->begin();
        if ($this->post($data)) {
            return $this->db->commit();
        }
        return $this->db->rollback();
    }

    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        $this->response->setHeader("Content-Type", "application/json");
        $this->view->disable();
        $this->response->send();
    }


}