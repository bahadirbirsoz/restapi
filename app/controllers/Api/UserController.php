<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 26.03.2017
 * Time: 22:44
 */

namespace Sony\Translate\Controllers\Api;

use Phalcon\Validation;
use Sony\Translate\Doc\UserPost;
use Sony\Translate\Doc\UserPut;
use Sony\Translate\Models\User;

/**
 * Class UserController
 * @package Sony\Translate\Controllers\Api
 */
class UserController extends Rest implements Rest\Get, Rest\Delete, Rest\Post, Rest\Put
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function deleteAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * @return boolean
     */
    public function delete($id):bool
    {
        $user = User::findFirst($id);
        if (!$user) {
            $this->response->errNotFound();
            return false;
        }
        $user->delete();
        return true;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function getAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * @return boolean
     */
    public function get($id = null):bool
    {
        if (!is_null($id)) {
            $data = User::findFirst($id);
        } else {
            $data = User::find();
        }
        if (!$data) {
            $this->response->errNotFound();
            return false;
        }
        $this->response->setJsonContent($data);
        return true;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function postAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * @param $data UserPost
     * @return bool
     */
    public function post($data):bool
    {
        $user = new User();
        $user->password = $this->security->hash($data->password);
        $user->email = $data->email;
        $user->role = $data->role;
        if ($user->save()) {
            return $this->get($user->user_id);
        }
        return false;
    }

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function postValidation(Validation $validation):Validation
    {

        $validation->add('email', new Validation\Validator\PresenceOf());
        $validation->add('password', new Validation\Validator\PresenceOf());
        $validation->add('role', new Validation\Validator\PresenceOf());
        $validation->add('email', new Validation\Validator\Email());
        $validation->add('password', new Validation\Validator\StringLength(['min' => 6]));
        $validation->add('role', new Validation\Validator\InclusionIn([
            'domain' => [User::ROLE_CONSUMER, User::ROLE_AGENCY, User::ROLE_ADMIN]
        ]));
        $validation->add('email', new Validation\Validator\Uniqueness([
            'model' => new User(),

        ]));
        return $validation;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function putAuth():bool
    {
        return $this->auth->isAdmin();
    }

    /**
     * @param $data UserPut
     * @return bool
     */
    public function put($data):bool
    {
        /* @var $user User */
        $user = User::findFirstByEmail($data->email);
        $user->role = $data->role;
        $user->password = $this->security->hash($data->password);
        if ($user->save()) {
            return $this->get($user->user_id);
        }
        return false;
    }

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function putValidation(Validation $validation):Validation
    {
        $validation->add('email', new Validation\Validator\PresenceOf());
        $validation->add('password', new Validation\Validator\PresenceOf());
        $validation->add('role', new Validation\Validator\PresenceOf());
        $validation->add('email', new Validation\Validator\Email());
        $validation->add('password', new Validation\Validator\StringLength(['min' => 6]));
        $validation->add('role', new Validation\Validator\InclusionIn([
            'domain' => [User::ROLE_CONSUMER, User::ROLE_AGENCY, User::ROLE_ADMIN]
        ]));
        $validation->add('email', new Validation\Validator\Callback([
            'message' => 'User cannot be cound in the database',
            'callback' => function ($data) {
                return (bool)User::findFirstByEmail($data->email);
            }
        ]));

        return $validation;
    }
}