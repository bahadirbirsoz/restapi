<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 25.03.2017
 * Time: 00:22
 */

namespace Sony\Translate\Controllers\Api;

use Phalcon\Validation;
use Sony\Translate\Doc\ValuePost;
use Sony\Translate\Library\Validator\LanguageCode;
use Sony\Translate\Models\Language;
use Sony\Translate\Models\Translation;

/**
 * Class ValueController
 * @package Sony\Translate\Controllers\Api
 */
class ValueController extends Rest implements Rest\Get, Rest\Post
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function getAuth():bool
    {
        return $this->auth->isAdmin() || $this->auth->isConsumer();
    }

    /**
     * Sets the translated values as response. If there is no translated values, sets the response as 404(Not Found)
     *
     * @return bool
     */
    public function get($id = null):bool
    {
        $data = $this->modelsManager->createBuilder()
            ->addFrom(Translation::class, 'translation')
            ->join(Language::class, 'sourcelang.language_id = translation.source_language_id', 'sourcelang')
            ->join(Language::class, 'targetlang.language_id = translation.target_language_id', 'targetlang')
            ->columns([
                'translation.source as value',
                'translation.target as translation',
                'sourcelang.code as source',
                'targetlang.code as target',
            ])
            ->where('translation.status = ?0', [Translation::STATUS_READY])
            ->getQuery()
            ->execute();

        if (!$data->count()) {
            $this->response->errNotFound();
            return true;
        }
        $this->response->setJsonContent($data->toArray());
        return true;
    }

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function postAuth():bool
    {
        return $this->auth->isAdmin() || $this->auth->isConsumer();
    }

    /**
     * @param $data ValuePost
     * @return bool
     */
    public function post($data):bool
    {
        /* @var $sourceLang Language */
        $sourceLang = Language::findFirstByCode($data->source);

        /* @var $targetLang Language */
        $targetLang = Language::findFirstByCode($data->target);

        $existingRecord = Translation::findFirst([
            'conditions' => 'target_language_id=?0 AND source_language_id = ?1 AND source = ?2',
            'bind' => [
                $targetLang->language_id,
                $sourceLang->language_id,
                $data->value
            ]
        ]);

        if ($existingRecord) {
            if ($existingRecord->status === Translation::STATUS_PENDING) {
                $this->response->error('Value is already in the translation queue', 'value', 409);
            } else {
                $this->response->error('Value is already translated', 'value', 409);
            }
            return false;
        }

        $translation = new Translation();
        $translation->source_language_id = $sourceLang->language_id;
        $translation->target_language_id = $targetLang->language_id;
        $translation->source = $data->value;
        if ($translation->save()) {
            return true;
        }
        $this->response->errMessages($translation->getMessages());
        return false;
    }

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function postValidation(Validation $validation):Validation
    {
        $validation->add('source', new Validation\Validator\PresenceOf());
        $validation->add('target', new Validation\Validator\PresenceOf());

        $validation->add('source', new LanguageCode('source'));
        $validation->add('target', new LanguageCode('target'));


        $validation->add('value', new Validation\Validator\PresenceOf());

        return $validation;
    }
}