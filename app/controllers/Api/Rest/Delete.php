<?php

namespace Sony\Translate\Controllers\Api\Rest;

/**
 * Interface Delete
 * @package Sony\Translate\Controllers\Api\Rest
 */
interface Delete
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return boolean
     */
    public function deleteAuth():bool;

    /**
     * Executed after authentication, authorization. Handles actual delete process.
     * @return boolean
     */
    public function delete($id):bool;

}

