<?php

namespace Sony\Translate\Controllers\Api\Rest;

use Phalcon\Validation;

/**
 * Interface Post for implementing HTTP POST requests
 *
 * @package Sony\Translate\Controllers\Api\Rest
 */
interface Post
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return boolean
     */
    public function postAuth():bool;

    /**
     * Executed after authentication, authorization. Handles actual post process.
     * @param mixed
     * @return boolean
     */
    public function post($data):bool;

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function postValidation(Validation $validation):Validation;

}


