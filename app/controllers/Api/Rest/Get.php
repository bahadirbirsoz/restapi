<?php

namespace Sony\Translate\Controllers\Api\Rest;

use Phalcon\Validation;

/**
 * Interface Get for implementing HTTP GET requests
 * @package Sony\Translate\Controllers\Api\Rest
 */
interface Get
{
    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return bool
     */
    public function getAuth():bool;

    /**
     * Executed after authentication, authorization. Handles actual get process.
     * @param null $id This argument can be used for either pagination or as unique identifier for targeted get requests
     * @return bool
     */
    public function get($id = null):bool;

}
