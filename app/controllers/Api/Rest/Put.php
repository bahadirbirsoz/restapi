<?php

namespace Sony\Translate\Controllers\Api\Rest;

use Phalcon\Validation;

/**
 * Interface Put for implementing HTTP PUT requests
 *
 * @package Sony\Translate\Controllers\Api\Rest
 */
interface Put
{

    /**
     * Returns true if user level have access to given operation, false otherwise
     * @return boolean
     */
    public function putAuth():bool;

    /**
     * Executed after authentication, authorization. Handles actual put process.
     * @param mixed
     * @return boolean
     */
    public function put($data):bool;

    /**
     * This method sets Validators (\Phalcon\Validation\Validator) for given Validation object
     * @param Validation $validation
     * @return Validation
     */
    public function putValidation(Validation $validation):Validation;

}
