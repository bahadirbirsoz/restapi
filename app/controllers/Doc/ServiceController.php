<?php
/**
 * Created by PhpStorm.
 * User: apple
 * Date: 28.03.2017
 * Time: 15:37
 */

namespace Sony\Translate\Controllers\Doc;

use Phalcon\Mvc\Controller;
use Sony\Translate\Controllers\Api\LanguageController;
use Sony\Translate\Controllers\Api\Rest;

class ServiceController extends Controller
{

    public function indexAction()
    {
        $this->view->title = "Sony Translation API";
        $this->view->methods = [];
        $this->view->endpoint = '';
    }

    public function languageAction()
    {
        $this->view->title = "Language Service";
        $this->view->endpoint = "/api/language";
        $this->view->methods = [
            "POST" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Creates new user.',
                'alert' => '',
                'request' => ['language' => 'string', 'code' => 'string:2'],
                'response' => '{"status":200,"response":{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date:Y-m-d H:i:s"}}',
            ],
            "GET" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Retrieves user list.',
                'alert' => 'If the language code is given at the endpoint, retrieves the language instead of language array.',
                'request' => [],
                'response' => '{"status":200,"response":[{"language_id":"int","language":"string","code":"string:2"},{"language_id":"int","language":"string","code":"string:2"}]}',
            ]
        ];
    }

    public function translateAction()
    {
        $this->view->title = "Translate Service";
        $this->view->endpoint = "/api/translate";
        $this->view->methods = [
            "POST" => [
                'canAdmin' => true,
                'canAgency' => true,
                'canConsumer' => false,
                'desc' => 'Redords translation of a value.',
                'alert' => 'Returns 404 if value is not found.',
                'request' => ['sourceLanguageCode' => 'string:2', 'targetLanguageCode' => 'string:2', 'sourceText' => 'string', 'targetText' => 'string'],
                'response' => '',
            ],

        ];

    }

    public function valueAction()
    {
        $this->view->title = "Values Service";
        $this->view->endpoint = "/api/value";
        $this->view->methods = [
            "POST" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => true,
                'desc' => 'Creates new record to be translated.',
                'alert' => 'Responses 409 with informative message if given value is already translated or pending to be translated',
                'request' => ['source' => 'string:2', 'target' => 'string:2', 'value' => 'string'],
                'response' => '{"status":200,"response":"Record inserted successfully"}',
            ],
            "GET" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => true,
                'desc' => 'Retrieves list of translated values.',
                'alert' => '',
                'request' => [],
                'response' => '{"status":200,"response":[{"value":"string","translation":"string","source":"string:2","target":"string:2"},{"value":"string","translation":"string","source":"string:2","target":"string:2"}]}',
            ],

        ];
    }

    public function userAction()
    {
        $this->view->title = "User Service";
        $this->view->endpoint = "/api/user";
        $this->view->methods = [
            "POST" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Creates new user.',
                'alert' => '',
                'request' => ['email' => 'string', 'password' => 'string', 'role' => 'string(admin|agency|consumer)'],
                'response' => '{"status":200,"response":{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date(Y-m-d H:i:s)"}}',
            ],
            "GET" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Retrieves user list.',
                'alert' => 'If an argument is passed, method return only one user data instead of user array',
                'request' => [],
                'response' => '{"status":200,"response":[{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date(Y-m-d H:i:s)"},{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date(Y-m-d H:i:s)"},{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date(Y-m-d H:i:s)"}]}',
            ],
            "PUT" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Updates user data of the given email address.',
                'alert' => '',
                'request' => ['email' => 'string', 'password' => 'string', 'role' => 'string(admin|agency|consumer)'],
                'response' => '{"status":200,"response":{"user_id":"int","email":"string","password":"string(hashed)","role":"enum(admin|agency|consumer)","time_register":"date(Y-m-d H:i:s)"}}',
            ],
            "DELETE" => [
                'canAdmin' => true,
                'canAgency' => false,
                'canConsumer' => false,
                'desc' => 'Deletes the user with given id on endpoint/[ID]',
                'alert' => '',
                'request' => [],
                'response' => '{"status":200,"response":"Record deleted successfully"}',
            ]
        ];
    }

    public function waitingAction()
    {
        $this->view->title = "Waiting Service";
        $this->view->endpoint = "/api/waiting";
        $this->view->methods = [
            "GET" => [
                'canAdmin' => true,
                'canAgency' => true,
                'canConsumer' => false,
                'desc' => 'Retrieves list of values to be translated.',
                'alert' => '',
                'request' => [],
                'response' => '{"status":200,"response":[{"value":"string","source":"string:2","target":"string:2"},{"value":"string","source":"string:2","target":"string:2"}]}',
            ],

        ];
    }


}