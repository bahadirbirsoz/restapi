
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Sony</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Translate</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">Controllers</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Sony_Translate_Controllers_Api" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Controllers/Api.html">Api</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Sony_Translate_Controllers_Api_Rest" >                    <div style="padding-left:72px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Controllers/Api/Rest.html">Rest</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Controllers_Api_Rest_Delete" >                    <div style="padding-left:98px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/Rest/Delete.html">Delete</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_Rest_Get" >                    <div style="padding-left:98px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/Rest/Get.html">Get</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_Rest_Post" >                    <div style="padding-left:98px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/Rest/Post.html">Post</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_Rest_Put" >                    <div style="padding-left:98px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/Rest/Put.html">Put</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_LanguageController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/LanguageController.html">LanguageController</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_Rest" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/Rest.html">Rest</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_TranslateController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/TranslateController.html">TranslateController</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_UserController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/UserController.html">UserController</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_ValueController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/ValueController.html">ValueController</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Api_WaitingController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Api/WaitingController.html">WaitingController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Sony_Translate_Controllers_Doc" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Controllers/Doc.html">Doc</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Controllers_Doc_IndexController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Doc/IndexController.html">IndexController</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Controllers_Doc_ServiceController" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Controllers/Doc/ServiceController.html">ServiceController</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Sony_Translate_Doc" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Doc.html">Doc</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Doc_LanguagePost" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Doc/LanguagePost.html">LanguagePost</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Doc_TranslatePost" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Doc/TranslatePost.html">TranslatePost</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Doc_UserPost" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Doc/UserPost.html">UserPost</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Doc_UserPut" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Doc/UserPut.html">UserPut</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Doc_ValuePost" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Doc/ValuePost.html">ValuePost</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Sony_Translate_Library" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Library.html">Library</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:Sony_Translate_Library_Validator" >                    <div style="padding-left:54px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Library/Validator.html">Validator</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Library_Validator_LanguageCode" >                    <div style="padding-left:80px" class="hd leaf">                        <a href="Sony/Translate/Library/Validator/LanguageCode.html">LanguageCode</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:Sony_Translate_Library_Auth" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Library/Auth.html">Auth</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Library_Response" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Library/Response.html">Response</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Sony_Translate_Models" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Models.html">Models</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Models_Language" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Models/Language.html">Language</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Models_Translation" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Models/Translation.html">Translation</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Models_User" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Models/User.html">User</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:Sony_Translate_Tasks" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="Sony/Translate/Tasks.html">Tasks</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:Sony_Translate_Tasks_InstallTask" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Tasks/InstallTask.html">InstallTask</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Tasks_MainTask" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Tasks/MainTask.html">MainTask</a>                    </div>                </li>                            <li data-name="class:Sony_Translate_Tasks_UserTask" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="Sony/Translate/Tasks/UserTask.html">UserTask</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "Sony.html", "name": "Sony", "doc": "Namespace Sony"},{"type": "Namespace", "link": "Sony/Translate.html", "name": "Sony\\Translate", "doc": "Namespace Sony\\Translate"},{"type": "Namespace", "link": "Sony/Translate/Controllers.html", "name": "Sony\\Translate\\Controllers", "doc": "Namespace Sony\\Translate\\Controllers"},{"type": "Namespace", "link": "Sony/Translate/Controllers/Api.html", "name": "Sony\\Translate\\Controllers\\Api", "doc": "Namespace Sony\\Translate\\Controllers\\Api"},{"type": "Namespace", "link": "Sony/Translate/Controllers/Api/Rest.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest", "doc": "Namespace Sony\\Translate\\Controllers\\Api\\Rest"},{"type": "Namespace", "link": "Sony/Translate/Controllers/Doc.html", "name": "Sony\\Translate\\Controllers\\Doc", "doc": "Namespace Sony\\Translate\\Controllers\\Doc"},{"type": "Namespace", "link": "Sony/Translate/Doc.html", "name": "Sony\\Translate\\Doc", "doc": "Namespace Sony\\Translate\\Doc"},{"type": "Namespace", "link": "Sony/Translate/Library.html", "name": "Sony\\Translate\\Library", "doc": "Namespace Sony\\Translate\\Library"},{"type": "Namespace", "link": "Sony/Translate/Library/Validator.html", "name": "Sony\\Translate\\Library\\Validator", "doc": "Namespace Sony\\Translate\\Library\\Validator"},{"type": "Namespace", "link": "Sony/Translate/Models.html", "name": "Sony\\Translate\\Models", "doc": "Namespace Sony\\Translate\\Models"},{"type": "Namespace", "link": "Sony/Translate/Tasks.html", "name": "Sony\\Translate\\Tasks", "doc": "Namespace Sony\\Translate\\Tasks"},
            {"type": "Interface", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "doc": "&quot;Interface Delete&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "fromLink": "Sony/Translate/Controllers/Api/Rest/Delete.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html#method_deleteAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete::deleteAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "fromLink": "Sony/Translate/Controllers/Api/Rest/Delete.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html#method_delete", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete::delete", "doc": "&quot;Executed after authentication, authorization. Handles actual delete process.&quot;"},
            
            {"type": "Interface", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "doc": "&quot;Interface Get for implementing HTTP GET requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "fromLink": "Sony/Translate/Controllers/Api/Rest/Get.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "fromLink": "Sony/Translate/Controllers/Api/Rest/Get.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get::get", "doc": "&quot;Executed after authentication, authorization. Handles actual get process.&quot;"},
            
            {"type": "Interface", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "doc": "&quot;Interface Post for implementing HTTP POST requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::post", "doc": "&quot;Executed after authentication, authorization. Handles actual post process.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Interface", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "doc": "&quot;Interface Put for implementing HTTP PUT requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_putAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::putAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_put", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::put", "doc": "&quot;Executed after authentication, authorization. Handles actual put process.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_putValidation", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::putValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController", "doc": "&quot;Class LanguageController&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_deleteAuth", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::deleteAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_delete", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::delete", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::post", "doc": "&quot;Handles language post operation.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\LanguageController", "fromLink": "Sony/Translate/Controllers/Api/LanguageController.html", "link": "Sony/Translate/Controllers/Api/LanguageController.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\LanguageController::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/Rest.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest", "doc": "&quot;Class Rest&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_initialize", "name": "Sony\\Translate\\Controllers\\Api\\Rest::initialize", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_getData", "name": "Sony\\Translate\\Controllers\\Api\\Rest::getData", "doc": "&quot;Returns the request parameters as an object&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_getAction", "name": "Sony\\Translate\\Controllers\\Api\\Rest::getAction", "doc": "&quot;Handles get action : Checks for authentication, starts a transaction, calls controller::get method\ncommits the transaction if get method returns true, rollback the transaction otherwise.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_putAction", "name": "Sony\\Translate\\Controllers\\Api\\Rest::putAction", "doc": "&quot;Handles put action : Checks for authentication, starts a transaction, calls controller::put method\ncommits the transaction if put method returns true, rollback the transaction otherwise.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_deleteAction", "name": "Sony\\Translate\\Controllers\\Api\\Rest::deleteAction", "doc": "&quot;Handles delete action : Checks for authentication, starts a transaction, calls controller::delete method\ncommits the transaction if delete method returns true, rollback the transaction otherwise.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_postAction", "name": "Sony\\Translate\\Controllers\\Api\\Rest::postAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest.html#method_afterExecuteRoute", "name": "Sony\\Translate\\Controllers\\Api\\Rest::afterExecuteRoute", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "doc": "&quot;Interface Delete&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "fromLink": "Sony/Translate/Controllers/Api/Rest/Delete.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html#method_deleteAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete::deleteAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete", "fromLink": "Sony/Translate/Controllers/Api/Rest/Delete.html", "link": "Sony/Translate/Controllers/Api/Rest/Delete.html#method_delete", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Delete::delete", "doc": "&quot;Executed after authentication, authorization. Handles actual delete process.&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "doc": "&quot;Interface Get for implementing HTTP GET requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "fromLink": "Sony/Translate/Controllers/Api/Rest/Get.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Get", "fromLink": "Sony/Translate/Controllers/Api/Rest/Get.html", "link": "Sony/Translate/Controllers/Api/Rest/Get.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Get::get", "doc": "&quot;Executed after authentication, authorization. Handles actual get process.&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "doc": "&quot;Interface Post for implementing HTTP POST requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::post", "doc": "&quot;Executed after authentication, authorization. Handles actual post process.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Post", "fromLink": "Sony/Translate/Controllers/Api/Rest/Post.html", "link": "Sony/Translate/Controllers/Api/Rest/Post.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Post::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest", "fromLink": "Sony/Translate/Controllers/Api/Rest.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "doc": "&quot;Interface Put for implementing HTTP PUT requests&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_putAuth", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::putAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_put", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::put", "doc": "&quot;Executed after authentication, authorization. Handles actual put process.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\Rest\\Put", "fromLink": "Sony/Translate/Controllers/Api/Rest/Put.html", "link": "Sony/Translate/Controllers/Api/Rest/Put.html#method_putValidation", "name": "Sony\\Translate\\Controllers\\Api\\Rest\\Put::putValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/TranslateController.html", "name": "Sony\\Translate\\Controllers\\Api\\TranslateController", "doc": "&quot;Class TranslateController&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\TranslateController", "fromLink": "Sony/Translate/Controllers/Api/TranslateController.html", "link": "Sony/Translate/Controllers/Api/TranslateController.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\TranslateController::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\TranslateController", "fromLink": "Sony/Translate/Controllers/Api/TranslateController.html", "link": "Sony/Translate/Controllers/Api/TranslateController.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\TranslateController::post", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\TranslateController", "fromLink": "Sony/Translate/Controllers/Api/TranslateController.html", "link": "Sony/Translate/Controllers/Api/TranslateController.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\TranslateController::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/UserController.html", "name": "Sony\\Translate\\Controllers\\Api\\UserController", "doc": "&quot;Class UserController&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_deleteAuth", "name": "Sony\\Translate\\Controllers\\Api\\UserController::deleteAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_delete", "name": "Sony\\Translate\\Controllers\\Api\\UserController::delete", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\UserController::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\UserController::get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\UserController::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\UserController::post", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\UserController::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_putAuth", "name": "Sony\\Translate\\Controllers\\Api\\UserController::putAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_put", "name": "Sony\\Translate\\Controllers\\Api\\UserController::put", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\UserController", "fromLink": "Sony/Translate/Controllers/Api/UserController.html", "link": "Sony/Translate/Controllers/Api/UserController.html#method_putValidation", "name": "Sony\\Translate\\Controllers\\Api\\UserController::putValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/ValueController.html", "name": "Sony\\Translate\\Controllers\\Api\\ValueController", "doc": "&quot;Class ValueController&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\ValueController", "fromLink": "Sony/Translate/Controllers/Api/ValueController.html", "link": "Sony/Translate/Controllers/Api/ValueController.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\ValueController::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\ValueController", "fromLink": "Sony/Translate/Controllers/Api/ValueController.html", "link": "Sony/Translate/Controllers/Api/ValueController.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\ValueController::get", "doc": "&quot;Sets the translated values as response. If there is no translated values, sets the response as 404(Not Found)&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\ValueController", "fromLink": "Sony/Translate/Controllers/Api/ValueController.html", "link": "Sony/Translate/Controllers/Api/ValueController.html#method_postAuth", "name": "Sony\\Translate\\Controllers\\Api\\ValueController::postAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\ValueController", "fromLink": "Sony/Translate/Controllers/Api/ValueController.html", "link": "Sony/Translate/Controllers/Api/ValueController.html#method_post", "name": "Sony\\Translate\\Controllers\\Api\\ValueController::post", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\ValueController", "fromLink": "Sony/Translate/Controllers/Api/ValueController.html", "link": "Sony/Translate/Controllers/Api/ValueController.html#method_postValidation", "name": "Sony\\Translate\\Controllers\\Api\\ValueController::postValidation", "doc": "&quot;This method sets Validators (\\Phalcon\\Validation\\Validator) for given Validation object&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Api", "fromLink": "Sony/Translate/Controllers/Api.html", "link": "Sony/Translate/Controllers/Api/WaitingController.html", "name": "Sony\\Translate\\Controllers\\Api\\WaitingController", "doc": "&quot;Class WaitingController&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\WaitingController", "fromLink": "Sony/Translate/Controllers/Api/WaitingController.html", "link": "Sony/Translate/Controllers/Api/WaitingController.html#method_getAuth", "name": "Sony\\Translate\\Controllers\\Api\\WaitingController::getAuth", "doc": "&quot;Returns true if user level have access to given operation, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Api\\WaitingController", "fromLink": "Sony/Translate/Controllers/Api/WaitingController.html", "link": "Sony/Translate/Controllers/Api/WaitingController.html#method_get", "name": "Sony\\Translate\\Controllers\\Api\\WaitingController::get", "doc": "&quot;There is no need to use $id argument for this method, always returns a list of values&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Doc", "fromLink": "Sony/Translate/Controllers/Doc.html", "link": "Sony/Translate/Controllers/Doc/IndexController.html", "name": "Sony\\Translate\\Controllers\\Doc\\IndexController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\IndexController", "fromLink": "Sony/Translate/Controllers/Doc/IndexController.html", "link": "Sony/Translate/Controllers/Doc/IndexController.html#method_indexAction", "name": "Sony\\Translate\\Controllers\\Doc\\IndexController::indexAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Controllers\\Doc", "fromLink": "Sony/Translate/Controllers/Doc.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_indexAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::indexAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_languageAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::languageAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_translateAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::translateAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_valueAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::valueAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_userAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::userAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Controllers\\Doc\\ServiceController", "fromLink": "Sony/Translate/Controllers/Doc/ServiceController.html", "link": "Sony/Translate/Controllers/Doc/ServiceController.html#method_waitingAction", "name": "Sony\\Translate\\Controllers\\Doc\\ServiceController::waitingAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Doc", "fromLink": "Sony/Translate/Doc.html", "link": "Sony/Translate/Doc/LanguagePost.html", "name": "Sony\\Translate\\Doc\\LanguagePost", "doc": "&quot;Class LanguagePost&quot;"},
                    
            {"type": "Class", "fromName": "Sony\\Translate\\Doc", "fromLink": "Sony/Translate/Doc.html", "link": "Sony/Translate/Doc/TranslatePost.html", "name": "Sony\\Translate\\Doc\\TranslatePost", "doc": "&quot;Class TranslatePost&quot;"},
                    
            {"type": "Class", "fromName": "Sony\\Translate\\Doc", "fromLink": "Sony/Translate/Doc.html", "link": "Sony/Translate/Doc/UserPost.html", "name": "Sony\\Translate\\Doc\\UserPost", "doc": "&quot;Class UserPost&quot;"},
                    
            {"type": "Class", "fromName": "Sony\\Translate\\Doc", "fromLink": "Sony/Translate/Doc.html", "link": "Sony/Translate/Doc/UserPut.html", "name": "Sony\\Translate\\Doc\\UserPut", "doc": "&quot;Class UserPut&quot;"},
                    
            {"type": "Class", "fromName": "Sony\\Translate\\Doc", "fromLink": "Sony/Translate/Doc.html", "link": "Sony/Translate/Doc/ValuePost.html", "name": "Sony\\Translate\\Doc\\ValuePost", "doc": "&quot;Class ValuePost&quot;"},
                    
            {"type": "Class", "fromName": "Sony\\Translate\\Library", "fromLink": "Sony/Translate/Library.html", "link": "Sony/Translate/Library/Auth.html", "name": "Sony\\Translate\\Library\\Auth", "doc": "&quot;Class Auth\nHandles HTTP Basic Authentication&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method___construct", "name": "Sony\\Translate\\Library\\Auth::__construct", "doc": "&quot;Auth constructor. Gets the auth header if exists.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_setUser", "name": "Sony\\Translate\\Library\\Auth::setUser", "doc": "&quot;Sets the User model for overriding. This method is to be used only in tests.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_getUser", "name": "Sony\\Translate\\Library\\Auth::getUser", "doc": "&quot;Returns User Object for current user&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_isAdmin", "name": "Sony\\Translate\\Library\\Auth::isAdmin", "doc": "&quot;Returns true if user is valid and admin, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_isAgency", "name": "Sony\\Translate\\Library\\Auth::isAgency", "doc": "&quot;Returns true if user is valid and agency, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_isConsumer", "name": "Sony\\Translate\\Library\\Auth::isConsumer", "doc": "&quot;Returns true if user is valid and consumer, false otherwise&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Auth", "fromLink": "Sony/Translate/Library/Auth.html", "link": "Sony/Translate/Library/Auth.html#method_check", "name": "Sony\\Translate\\Library\\Auth::check", "doc": "&quot;Returns true if user is valid, false otherwise&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Library", "fromLink": "Sony/Translate/Library.html", "link": "Sony/Translate/Library/Response.html", "name": "Sony\\Translate\\Library\\Response", "doc": "&quot;Class Response&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_setDefaultContent", "name": "Sony\\Translate\\Library\\Response::setDefaultContent", "doc": "&quot;Sets default content.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errNotFound", "name": "Sony\\Translate\\Library\\Response::errNotFound", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errMessages", "name": "Sony\\Translate\\Library\\Response::errMessages", "doc": "&quot;This method handles error messages after validation fails.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_error", "name": "Sony\\Translate\\Library\\Response::error", "doc": "&quot;General purpose error response handler.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errAuthFailed", "name": "Sony\\Translate\\Library\\Response::errAuthFailed", "doc": "&quot;Handles response when user is not authorized to perform requested method.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errForbidden", "name": "Sony\\Translate\\Library\\Response::errForbidden", "doc": "&quot;Handles response when given authorization information is cannot be found in database or invalid.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errInvalidAction", "name": "Sony\\Translate\\Library\\Response::errInvalidAction", "doc": "&quot;Handles response when an invalid action has been requested.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_errInvalidRequest", "name": "Sony\\Translate\\Library\\Response::errInvalidRequest", "doc": "&quot;Handles response when request body for given method cannot be handled (Invalid JSON or x-http form data)&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_success", "name": "Sony\\Translate\\Library\\Response::success", "doc": "&quot;Handles general purpose success messages&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_setJsonContent", "name": "Sony\\Translate\\Library\\Response::setJsonContent", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Library\\Response", "fromLink": "Sony/Translate/Library/Response.html", "link": "Sony/Translate/Library/Response.html#method_send", "name": "Sony\\Translate\\Library\\Response::send", "doc": "&quot;Sends the response to client.&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Library\\Validator", "fromLink": "Sony/Translate/Library/Validator.html", "link": "Sony/Translate/Library/Validator/LanguageCode.html", "name": "Sony\\Translate\\Library\\Validator\\LanguageCode", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Library\\Validator\\LanguageCode", "fromLink": "Sony/Translate/Library/Validator/LanguageCode.html", "link": "Sony/Translate/Library/Validator/LanguageCode.html#method___construct", "name": "Sony\\Translate\\Library\\Validator\\LanguageCode::__construct", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Models", "fromLink": "Sony/Translate/Models.html", "link": "Sony/Translate/Models/Language.html", "name": "Sony\\Translate\\Models\\Language", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Models\\Language", "fromLink": "Sony/Translate/Models/Language.html", "link": "Sony/Translate/Models/Language.html#method_initialize", "name": "Sony\\Translate\\Models\\Language::initialize", "doc": "&quot;Initialize method for model.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Language", "fromLink": "Sony/Translate/Models/Language.html", "link": "Sony/Translate/Models/Language.html#method_find", "name": "Sony\\Translate\\Models\\Language::find", "doc": "&quot;Allows to query a set of records that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Language", "fromLink": "Sony/Translate/Models/Language.html", "link": "Sony/Translate/Models/Language.html#method_findFirst", "name": "Sony\\Translate\\Models\\Language::findFirst", "doc": "&quot;Allows to query the first record that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Language", "fromLink": "Sony/Translate/Models/Language.html", "link": "Sony/Translate/Models/Language.html#method_getSource", "name": "Sony\\Translate\\Models\\Language::getSource", "doc": "&quot;Returns table name mapped in the model.&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Models", "fromLink": "Sony/Translate/Models.html", "link": "Sony/Translate/Models/Translation.html", "name": "Sony\\Translate\\Models\\Translation", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_initialize", "name": "Sony\\Translate\\Models\\Translation::initialize", "doc": "&quot;Initialize method for model.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_find", "name": "Sony\\Translate\\Models\\Translation::find", "doc": "&quot;Allows to query a set of records that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_findFirst", "name": "Sony\\Translate\\Models\\Translation::findFirst", "doc": "&quot;Allows to query the first record that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_findPending", "name": "Sony\\Translate\\Models\\Translation::findPending", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_findReady", "name": "Sony\\Translate\\Models\\Translation::findReady", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\Translation", "fromLink": "Sony/Translate/Models/Translation.html", "link": "Sony/Translate/Models/Translation.html#method_getSource", "name": "Sony\\Translate\\Models\\Translation::getSource", "doc": "&quot;Returns table name mapped in the model.&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Models", "fromLink": "Sony/Translate/Models.html", "link": "Sony/Translate/Models/User.html", "name": "Sony\\Translate\\Models\\User", "doc": "&quot;Class User&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Models\\User", "fromLink": "Sony/Translate/Models/User.html", "link": "Sony/Translate/Models/User.html#method_initialize", "name": "Sony\\Translate\\Models\\User::initialize", "doc": "&quot;Initialize method for model.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\User", "fromLink": "Sony/Translate/Models/User.html", "link": "Sony/Translate/Models/User.html#method_find", "name": "Sony\\Translate\\Models\\User::find", "doc": "&quot;Allows to query a set of records that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\User", "fromLink": "Sony/Translate/Models/User.html", "link": "Sony/Translate/Models/User.html#method_findFirst", "name": "Sony\\Translate\\Models\\User::findFirst", "doc": "&quot;Allows to query the first record that match the specified conditions&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\User", "fromLink": "Sony/Translate/Models/User.html", "link": "Sony/Translate/Models/User.html#method_getSource", "name": "Sony\\Translate\\Models\\User::getSource", "doc": "&quot;Returns table name mapped in the model.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Models\\User", "fromLink": "Sony/Translate/Models/User.html", "link": "Sony/Translate/Models/User.html#method_beforeValidationOnCreate", "name": "Sony\\Translate\\Models\\User::beforeValidationOnCreate", "doc": "&quot;Hooks the create event before validation to set time_register&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Tasks", "fromLink": "Sony/Translate/Tasks.html", "link": "Sony/Translate/Tasks/InstallTask.html", "name": "Sony\\Translate\\Tasks\\InstallTask", "doc": "&quot;Class InstallTask&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\InstallTask", "fromLink": "Sony/Translate/Tasks/InstallTask.html", "link": "Sony/Translate/Tasks/InstallTask.html#method_mainAction", "name": "Sony\\Translate\\Tasks\\InstallTask::mainAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\InstallTask", "fromLink": "Sony/Translate/Tasks/InstallTask.html", "link": "Sony/Translate/Tasks/InstallTask.html#method_canConnect", "name": "Sony\\Translate\\Tasks\\InstallTask::canConnect", "doc": "&quot;Checks if current connection arguments are valid and working&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\InstallTask", "fromLink": "Sony/Translate/Tasks/InstallTask.html", "link": "Sony/Translate/Tasks/InstallTask.html#method_getConnectionParams", "name": "Sony\\Translate\\Tasks\\InstallTask::getConnectionParams", "doc": "&quot;Reads connection arguments from user.&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\InstallTask", "fromLink": "Sony/Translate/Tasks/InstallTask.html", "link": "Sony/Translate/Tasks/InstallTask.html#method_save", "name": "Sony\\Translate\\Tasks\\InstallTask::save", "doc": "&quot;Saves config file&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Tasks", "fromLink": "Sony/Translate/Tasks.html", "link": "Sony/Translate/Tasks/MainTask.html", "name": "Sony\\Translate\\Tasks\\MainTask", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\MainTask", "fromLink": "Sony/Translate/Tasks/MainTask.html", "link": "Sony/Translate/Tasks/MainTask.html#method_mainAction", "name": "Sony\\Translate\\Tasks\\MainTask::mainAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "Sony\\Translate\\Tasks", "fromLink": "Sony/Translate/Tasks.html", "link": "Sony/Translate/Tasks/UserTask.html", "name": "Sony\\Translate\\Tasks\\UserTask", "doc": "&quot;Class UserTask&quot;"},
                                                        {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\UserTask", "fromLink": "Sony/Translate/Tasks/UserTask.html", "link": "Sony/Translate/Tasks/UserTask.html#method_mainAction", "name": "Sony\\Translate\\Tasks\\UserTask::mainAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\UserTask", "fromLink": "Sony/Translate/Tasks/UserTask.html", "link": "Sony/Translate/Tasks/UserTask.html#method_isValid", "name": "Sony\\Translate\\Tasks\\UserTask::isValid", "doc": "&quot;Checks if given user information validates&quot;"},
                    {"type": "Method", "fromName": "Sony\\Translate\\Tasks\\UserTask", "fromLink": "Sony/Translate/Tasks/UserTask.html", "link": "Sony/Translate/Tasks/UserTask.html#method_getArguments", "name": "Sony\\Translate\\Tasks\\UserTask::getArguments", "doc": "&quot;Gets user information to create from cli&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


